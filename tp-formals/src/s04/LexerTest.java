package s04;

import java.util.ArrayList;

public class LexerTest {
  static class TestData{
    final String expr;
    final int    size;  // number of tokens
    TestData(int s, String e) {
      expr=e; size=s;
    }
  }
  //============================================================
  static ArrayList<TestData> corpus=new ArrayList<TestData>();
  
  static {
    corpus.add(new TestData(1, "7" ));
    corpus.add(new TestData(3, "(2)" ));
    corpus.add(new TestData(3, "3+21" ));
    corpus.add(new TestData(5, "2*2+3"  ));
    corpus.add(new TestData(5, " 2  * 2+ 3"  ));
    corpus.add(new TestData(5, "2  *2+ 3 "  ));
    corpus.add(new TestData(8, "((abs(sqr(2)"));
    corpus.add(new TestData(7, "(sqrt (4)))" ));
    corpus.add(new TestData(6, "aa55bb cc 4 4"));
    corpus.add(new TestData(11, "2+2+2+2+2+2"));
  }

  public static void testLexicalAnalyzer(String expr, int expectedSize) {
    System.out.println("--------------------");
    System.out.println("Symbol stream for : '" + expr + "'");
    Lexer s = new Lexer(expr);
    System.out.print("  gives : ");
    int n = 0;
    String whole = "";
    String t = s.crtSymbol();
    while (!t.equals("")) {
      n++;
      whole += t;
      System.out.print(t + " [" + s.crtSymbolKind() + "] ");
      s.goToNextSymbol();
      t=s.crtSymbol();
    }
    System.out.println();   
    if (n!=expectedSize) 
      generateException("bad number of symbols");
    if (!whole.equals(withoutSpaces(expr))) 
      generateException("bad sequence of symbols");
  }

  public static void generateException(String msg) {
    try { Thread.sleep(300); }
    catch (InterruptedException ex) { }
    throw new RuntimeException(msg);
  }

  static String withoutSpaces(String s) {
    String res="";
    for(int i=0; i<s.length(); i++) {
      char c=s.charAt(i);
      if(c != ' ') res += c;
    }
    return res;
  }
  
  // ------------------------------------------------------------
  public static void main(String[] args) {
    for (TestData e:corpus) {
      testLexicalAnalyzer(e.expr, e.size);
    }
    System.out.println("\nTest passed successfully !");
  }
}
