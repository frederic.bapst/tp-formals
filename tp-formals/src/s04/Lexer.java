package s04;

import static s04.Lexer.SymbolKind.*;

public class Lexer {
  public enum SymbolKind {
    OPEN_PARENTH, CLOSE_PARENTH, PLUS, MINUS, STAR, SLASH, 
    NUMBER, IDENTIFIER, NONE, UNKNOWN
  }
  //======================================================
  private String crtToken = "";
  private final String whole;
  private int nextCharIndex = 0;  // index of the char following crtToken

  public Lexer(String s) {
    whole = s;
    goToNextSymbol();
  }

  public String crtSymbol() {   // returns "" if the end is reached
    return crtToken;
  }

  private boolean noMoreChars() {
    return nextCharIndex >= whole.length();
  }

  private void trimSpaces() {
    if (noMoreChars()) return;
    char c = whole.charAt(nextCharIndex);
    while (Character.isWhitespace(c)) { 
      nextCharIndex++;
      if (noMoreChars()) return;
      c = whole.charAt(nextCharIndex);
    }
    return;    
  }

  public void goToNextSymbol() {
    crtToken = "";
    trimSpaces();
    if (noMoreChars()) return;
    char c = whole.charAt(nextCharIndex);
    // .....
    // TODO - A COMPLETER...
    // Selon que le prochain caract�re est :
    // - une lettre : 
    //     grouper les lettres cons�cutives
    // - un chiffre : 
    //     grouper les chiffres cons�cutifs
    // - autre chose : 
    //     prendre juste ce caract�re
  }

  public SymbolKind crtSymbolKind() {
    String s = crtSymbol();
    switch(s) {
      case ""  : return NONE;
      case "(" : return OPEN_PARENTH;
      case ")" : return CLOSE_PARENTH;
      case "+" : return PLUS;
      case "-" : return MINUS;
      case "*" : return STAR;
      case "/" : return SLASH;
    }
    boolean allDigits = true;
    boolean allLetters = true;
    for(char c: s.toCharArray()) {
      if(!Character.isLetter(c)) allLetters = false;
      if(!Character.isDigit(c))  allDigits  = false;
    }
    if(allDigits)  return NUMBER;
    if(allLetters) return IDENTIFIER;
    return UNKNOWN;
  }

  // returns 0 if crtSymbol is not a number
  public int intFromSymbol() { 
    if (crtSymbolKind() != NUMBER) 
      return 0;
    return Integer.parseInt(crtToken);
  }

}
